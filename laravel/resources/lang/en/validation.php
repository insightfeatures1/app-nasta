<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ' :attribute trebuie sa fie acceptat.',
    'active_url'           => ' :attribute nu este o adresă URL validă.',
    'after'                => ' :attribute trebuie sa fie o data dupa :date.',
    'alpha'                => ' :attribute poate conține numai litere.',
    'alpha_dash'           => ' :attribute poate conține numai litere, numere și liniuțe.',
    'alpha_num'            => ' :attribute poate conține numai litere și numere.',
    'array'                => ' :attribute trebuie să fie o matrice.',
    'before'               => ' :attribute trebuie să fie o dată înainte de :date.',
    'between'              => [
        'numeric' => ' :attribute trebuie sa fie intre :min - :max.',
        'file'    => ' :attribute trebuie sa fie intre :min - :max kilobytes.',
        'string'  => ' :attribute trebuie sa fie intre :min - :max caractere.',
        'array'   => ' :attribute trebuie sa fie intre :min - :max elemente.',
    ],
    'boolean'              => 'Campul :attribute trebuie să fie adevărat sau fals.',
    'confirmed'            => 'Confirmarea :attribute nu se potrivește.',
    'date'                 => ' :attribute nu este o dată validă.',
    'date_format'          => ' :attribute nu se potriveste cu :format.',
    'different'            => ' :attribute si :other trebuie sa fie diferite.',
    'digits'               => ' :attribute trebuie sa aibe :digits cifre.',
    'digits_between'       => ' :attribute trebuie sa fie intre :min - :max cifre.',
    'distinct'             => ' Campul :attribute are o valoare duplicat.',
    'email'                => ' Introduceți o adresă de :attribute validă.',
    'exists'               => ' :attribute selectat este nevalid.',
    'filled'               => 'Campul :attribute este necesar.',
    'image'                => ' :attribute trebuie să fie o imagine.',
    'in'                   => ' :attribute selectat este nevalid.',
    'in_array'             => 'Campul :attribute nu exista in :other.',
    'integer'              => ' :attribute trebuie să fie un număr întreg.',
    'ip'                   => ' :attribute trebuie să fie o adresă IP validă.',
    'json'                 => ' :attribute trebuie să fie un șir JSON valid.',
    'max'                  => [
        'numeric' => ' :attribute nu poate fi mai mare decât :max.',
        'file'    => ' :attribute nu poate fi mai mare decât :max kilobytes.',
        'string'  => ' :attribute nu poate fi mai mare decât :max caracatere.',
        'array'   => ' :attribute nu poate avea mai mult de :max elemente.',
    ],
    'mimes'                => ' :attribute trebuie sa fie un fisier de type: :values.',
    'min'                  => [
        'numeric' => ' :attribute trebuie sa aibă cel putin :min.',
        'file'    => ' :attribute trebuie sa aibă cel putin :min kilobytes.',
        'string'  => ' :attribute trebuie sa aibă cel putin :min caractere.',
        'array'   => ' :attribute trebuie să aibă cel puțin :min elemente.',
    ],
    'not_in'               => ' :attribute selectat este nevalid.',
    'numeric'              => ' :attribute trebuie să fie un număr.',
    'present'              => ' :attribute trebuie să fie prezent.',
    'regex'                => ' :attribute formatul este nevalid.',
    'required'             => ' Vă rugăm să completați câmpul :attribute.',
    'required_if'          => ' Câmpul :attribute este necesar cand :other este :value.',
    'required_unless'      => ' :attribute este necesar daca :other este in :values.',
    'required_with'        => ' :attribute este necesar cand exista :values .',
    'required_with_all'    => ' :attribute este necesar cand exista :values .',
    'required_without'     => ' :attribute este necesar cand nu exista :values .',
    'required_without_all' => ' :attribute este necesar atunci când niciuna dintre :values nu exista.',
    'same'                 => ' :attribute si :other trebuie să se potrivească.',
    'size'                 => [
        'numeric' => ' :attribute trebuie sa fie :size.',
        'file'    => ' :attribute trebuie sa fie :size kilobytes.',
        'string'  => ' :attribute trebuie sa aibe :size caractere.',
        'array'   => ' :attribute trebuie sa contina :size elemente.',
    ],
    'string'               => ' :attribute trebuie să fie un șir.',
    'timezone'             => ' :attribute trebuie să fie o zonă validă.',
    'unique'               => ' Exista un cont creat cu aceasta adresa de :attribute .',
    'url'                  => ' :attribute este nevalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */


    'custom' => [
        'agree'         => [
            'required' => 'Este nevoie să bifați "Sunt de acord cu Termenii și condițiile"',
        ],
        'd.postal_code' => [
            'required' => 'Este nevoie să completati codul poștal',
            'numeric'  => 'Codul postal trebuie sa contina doar cifre',
        ],
        'd.city'        => [
            'required' => 'Este nevoie să completati orasul',
        ],

        'd.ap' => [
            'required' => 'Este nevoie să completati numărul apartamentului',
            'numeric'  => 'Numarul de apartament trebuie sa contina doar cifre',
        ],

        'd.bl' => [
            'required' => 'Este nevoie să completati blocul',
            'numeric'  => 'Blocul trebuie sa contina doar cifre',
        ],

        'd.no'          => [
            'required' => 'Este nevoie să completati numărul strazii',
            'numeric'  => 'Numarul strazii trebuie sa contina doar cifre',
        ],
        'd.street'      => [
            'required' => 'Este nevoie să completati strada',
        ],
        'i.postal_code' => [
            'required' => 'Este nevoie să completati codul poștal',
            'numeric'  => 'Codul postal trebuie sa contina doar cifre',
        ],
        'i.city'        => [
            'required' => 'Este nevoie să completati orasul',
        ],

        'i.ap'                 => [
            'required' => 'Este nevoie să completati numărul apartamentului',
            'numeric'  => 'Numarul de apartament trebuie sa contina doar cifre',
        ],
        'i.bl'                 => [
            'required' => 'Este nevoie să completati blocul',
            'numeric'  => 'Blocul trebuie sa contina doar cifre',
        ],
        'i.no'                 => [
            'required' => 'Este nevoie să completati numărul strazii',
            'numeric'  => 'Numarul trebuie sa contina doar cifre',
        ],
        'i.street'             => [
            'required' => 'Este nevoie să completati strada',
        ],
        'email'                => [
            'required' => 'Este nevoie să completati adresă de email',
        ],
        'password'             => [
            'required' => 'Este nevoie să completati parola',
        ],
        'first_name'           => [
            'required' => 'Este nevoie să completati prenumele',
        ],
        'last_name'            => [
            'required' => 'Este nevoie să completati numele',
        ],
        'phone'                => [
            'required' => 'Este nevoie să completati telefonul',
            'numeric'  => 'Numarul de telefon trebuie sa contina doar cifre',
        ],
        'details.first_name'   => [
            'required' => 'Este nevoie sa completati prenumele',
        ],
        'details.last_name'    => [
            'required' => 'Este nevoie sa completati numele',
        ],
        'details.phone'        => [
            'required' => 'Este nevoie sa completati telefonul',
            'numeric'  => 'Numarul de telefon trebuie sa contina doar cifre',
        ],
        'details.email'        => [
            'required' => 'Este nevoie sa completati emailul',
            'email'    => 'Adresa de email nu este valida',
        ],
        'd.county'             => [
            'required' => 'Este nevoie sa completati judetul',
        ],
        'g-recaptcha-response' => [
            'required' => 'Vericati ca nu sunteti un robot prin bifarea casutei.',
            'captcha'  => 'Eroare captcha! Incercati din nou.',
        ],


    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'contact' => [
            'mesaj' => 'Mesaj',
            'name'  => 'Nume',
            'email' => 'Email',
            'phone' => 'Telefon',
        ],
    ],

];
