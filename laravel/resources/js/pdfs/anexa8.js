// playground requires you to assign document definition to a variable called dd

const anexa8 = (pacient) => {
    const zonaUrban = pacient.zona === 'urban' ? ' x ' : ' ';
    const zonaRural = pacient.zona === 'rural' ? ' x ' : ' ';
    const masculin = pacient.gen === 'masculin' ? ' x ' : ' ';
    const feminin = pacient.gen === 'feminin' ? ' x ' : ' ';

    const varsta1 = pacient.varsta === 'sub-25' ? ' x ' : ' ';
    const varsta2 = pacient.varsta === '25-54' ? ' x ' : ' ';
    const varsta3 = pacient.varsta === 'peste-54' ? ' x ' : ' ';

    const situatia1 = pacient.informatii_generale.situatie === 'Angajat' ? ' x ' : ' ';
    const situatia2 = pacient.informatii_generale.situatie === "Somer" ? ' x ' : ' ';
    const situatia3 = pacient.informatii_generale.situatie === "Persoana-inactiva-(inclusiv-copii-anteprescolari-prescolari-elevi-etc.)" ? ' x ' : ' ';
    const situatia4 = pacient.informatii_generale.situatie === "Angajat-pe-cont-propriu" ? ' x ' : ' ';
    const situatia5 = pacient.informatii_generale.situatie === "Somer-de-lunga-durata" ? ' x ' : ' ';
    const situatia6 = pacient.informatii_generale.situatie === "Alta-categorie-de-inactivi-in-afara-de-cei-din-educatie-si-formare" ? ' x ' : ' ';
    const situatia7 = pacient.informatii_generale.situatie === 'Detinut' ? ' x ' : ' ';
    
    return {
        content: [
            {
                text: [
                    {
                        text: "PROGRAMUL OPERATIONAL CAPITAL UMAN",
                        bold: true
                    },
                    "\n Axa prioritara 4 - Incluziunea socială și combaterea saraciei \n Obiectivul tematic 9: Promovarea incluziunii sociale, combaterea saraciei și a oricarei forme de discriminare\n Prioritatea de investitii 9.iv: Cresterea accesului la servicii accesibile, durabile și de inalta calitate, inclusiv asistenta medicala și servicii sociale de interes general \n Obiectivul specific 4.9: Creșterea numărului de persoane care beneficiază de programe de sănătate și de servicii orientate către prevenție, depistare precoce (screening), diagnostic și tratament precoce pentru principalele patologii și și \n Titlul proiectului: ",
                    {
                        text:
                            "Organizarea de programe de depistare precoce(screening), diagnostic și tratament precoce al tuberculozei, inclusiv al tuberculozei latente",
                        bold: true
                    },
                    "\n Contract nr.POCU / 225 / 4 / 9 / 117426(Cod SMIS 2014 +: 117426) \n\n\n\n"
                ],
                style: "header"
            },
            {
                text:
                    "[Pentru referința cu anexele la Manualul Beneficiarului pentru formularul de mai jos se va păstra numerotarea stabilită în Manualul Beneficiarului]\n",
                style: "subheader"
            },
            { text: "ANEXA 8\n\n\n", fontSize: 11, alignment: "right" },
            {
                text:
                    "Formularul de înregistrare individuală a participanților \n la operațiunile finanțate prin POCU 2014 - 2020\n\n\n",
                alignment: "center",
                bold: true
            },
            {
                text: [
                    { text: "Cod SMIS proiect: ", bold: true },
                    "117426 \n\n",
                    { text: "Axă prioritară: ", bold: true },
                    "Incluziunea socială și combaterea sărăciei\n\n",
                    { text: "Titlu proiect: ", bold: true },
                    "Organizarea de programe de depistre precoce(screening), diagnostic și tratament precoce al tuberculozei, inclusiv al tuberculozei latente \n\n",
                    { text: "OIR/OI responsabil: ", bold: true },
                    "București Ilfov\n\n\n"
                ]
            },
            {
                text: "Secțiunea A. La intrarea în operațiune\n\n",
                bold: true,
                decoration: "underline"
            },
            {
                text: [
                    { text: "Date de contact: ", bold: true },
                    `${pacient.nume} ${pacient.prenume}, ${pacient.oras} ${pacient.judet}, ${pacient.telefon}, ${pacient.email}\n\n`,
                    {
                        text:
                            "[Nume, prenume, adresadomiciliu, locul de reședință, telefon, e - mail]\n\n",
                        italics: true,
                        alignment: "right",
                        fontSize: 9
                    },
                    "",
                    {
                        text: `Data intrării în operațiune: ${pacient.created_at}`,
                        bold: true
                    },
                    "……………………………………………..\n\n",
                    { text: `CNP: ${pacient.cnp}`, bold: true },
                    "……………………………………………..\n\n",
                    {
                        text: `Zonă: \n`,
                        bold: true
                    },
                    { text: `[${zonaUrban}] `, style: "checkbox" },

                    {
                        text: "Urban \n"
                    },

                    { text: `[${zonaRural}] `, style: "checkbox" },

                    {
                        text: "Rural \n\n"
                    },

                    {
                        text: "Localizare geografică:\n",
                        bold: true
                    },
                    "Regiune:………………………………………..\n",
                    "Județ:…………………………………………\n",
                    "Unitate teritorial administrativă: ……………….\n\n",
                    {
                        text: "Gen: \n",
                        bold: true
                    },
                    { text: `[${masculin}] `, style: "checkbox" },
                    {
                        text: "Masculin \n"
                    },
                    { text: `[${feminin}] `, style: "checkbox" },
                    { text: "Feminin \n\n\n\n" },
                    {
                        text: "Vârsta: \n",
                        bold: true
                    },
                    { text: `[${varsta1}] `, style: "checkbox" },
                    {
                        text: "Persoană cu vârsta sub 25 ani\n"
                    },
                    { text: `[${varsta2}] `, style: "checkbox" },
                    {
                        text: "Persoană cu vârsta cuprinsă între 25 și 54ani \n"
                    },
                    { text: `[${varsta3}] `, style: "checkbox" },
                    {
                        text: "Persoană cu vârsta peste 54 de ani\n\n"
                    },
                    {
                        text: "Categoria de Grup Țintă din care face parte:",
                        bold: true
                    },
                    "persoane din mediulrural\n\n",
                    {
                        text: "Situația pe piața forței de muncă persoană ocupată: \n",
                        bold: true
                    },
                    { text: `[${situatia1}] `, style: "checkbox" },
                    {
                        text: "Angajat \n"
                    },
                    { text: `[${situatia2}] `, style: "checkbox" },
                    {
                        text: "Angajat pe cont propriu \n"
                    },
                    { text: `[${situatia3}] `, style: "checkbox" },
                    {
                        text: "Șomer \n"
                    },
                    { text: `[${situatia4}] `, style: "checkbox" },
                    {
                        text: "Șomer de lungă durată \n"
                    },
                    { text: `[${situatia5}] `, style: "checkbox" },
                    {
                        text:
                            "Persoană inactivă (inclusiv copii antepreșcolari, preșcolari, elevi etc.) \n"
                    },
                    { text: `[${situatia6}] `, style: "checkbox" },
                    {
                        text:
                            "Alta categorie de inactivi în afara de cei din educație și formare \n\n"
                    },
                    { text: `[${situatia7}] `, style: "checkbox" },
                    {
                        text:
                            "Detinut \n\n"
                    },
                    {
                        text: "Nivel de educație: \n",
                        bold: true
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Studii Educație timpurie (ISCED 0)\n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Studii primare (ISCED 1) \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Studii gimnaziale (ISCED 2) \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Studii liceale (ISCED 3) \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Studii postliceale (ISCED 4) \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Studii superioare (ISCED 5) \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Studii superioare (ISCED 6) \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Studii superioare (ISCED 7) \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Studii superioare (ISCED 8) \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "fără ISCED \n\n"
                    },
                    {
                        text: "Persoană dezavantajată: \n",
                        bold: true
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "DA \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "NU \n\n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Participanți care trăiesc în gospodării fără persoane ocupate \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Participanți care trăiesc în gospodării fără persoane ocupate cu copii aflați în întreținere \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Participanți care trăiesc în gospodării alcătuite dintr - un părinte unic cu copil aflat în întreținere \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Migranți \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Participanți de origine străină\n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Minorități \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Etnie romă \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Alta minoritate decât cea de etnie romă\n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Comunități marginalizate \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Participanți cu dizabilități \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Alte categorii defavorizate \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Persoane fără adăpost sau care sunt afectate de excluziunea locativă \n"
                    },
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Niciuna din opțiunile de mai sus\n\n"
                    }
                ]
            },
            {
                alignment: "justify",
                columns: [
                    {
                        text: [
                            "Semnătura participant\n\n\n",
                            "……………………\n\n",
                            "Data:\n\n",
                            "……………………"
                        ],
                        bold: true
                    },
                    {
                        text: [
                            "Semnătura responsabil cu înregistrarea participanților\n\n",
                            "……………………\n\n",
                            "Data:\n\n",
                            "……………………"
                        ],
                        bold: true
                    }
                ]
            },
            {
                text: "\n\n\nNotă: Prin completarea și semnarea acestui formular vă exprimați consimțământul cu privire la utilizarea și prelucrarea datelor personale.",
                fontSize: 11,
                decoration: "underline"
            },
            {

                text:

                    "Datele dumneavoastră personale, înregistrate / colectate în cadrul derulării proiectelor POCU, nu sunt prelucrate în niciun alt scop în afară de cele menționate în Formularul de înregistare individuală și nu sunt comunicate către niciun terț, excepție făcând doar instituțiile / autoritățile publice, conform prevederilor legale în vigoare.",
                fontSize: 11,
                italics: true
            },

            {

                text: "\n\n\nSecțiunea B. La ieșirea din operațiune\n\n\n",

                decoration: "underline",

                bold: true

            },

            {

                text: [

                    {
                        text: "Dată ieșire din operațiune: ……………………..\n\n\n", bold: true
                    },

                    {
                        text: "Situația pe piața forței de muncă: \n\n", bold: true
                    },

                    { text: `${situatia1}`, style: "checkbox" },

                    {
                        text: "Angajat \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Angajat pe cont propriu \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Șomer \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoană inactivă angajată în căutarea unui loc de muncă la încetarea calității de participant \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoană care urmează studii/ cursuri de formare la încetarea calității de participant \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoană care dobândește o calificare la încetarea calității de participant \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoană care are un loc de muncă la încetarea calității de participant \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoană care desfășoară o activitate independentă la încetarea calității de participant \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoană defavorizată angajată în căutarea unui loc de muncă la încetarea calității de participant \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoană defavorizată angajată implicată în educație / formare la încetarea calității de participant \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoană defavorizată angajată în dobândirea unei calificări la încetarea calității de participant \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoană defavorizată care are un loc de muncă, la încetarea calității de participant \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoană defavorizată desfășoară o activitate independentă, la încetarea calității de participant \n\n"

                    },

                    { text: "Nivel de educație: :\n\n", bold: true },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Studii Educație timpurie (ISCED 0) \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Studii primare (ISCED 1) \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Studii gimnaziale (ISCED 2) \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Studii liceale (ISCED 3) \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Studii postliceale (ISCED 4) \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Studii superioare (ISCED 5) \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Studii superioare (ISCED 6) \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Studii superioare (ISCED 7) \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Studii superioare (ISCED 8) \n\n"
                    }

                ]

            },

            {

                alignment: "justify",

                columns: [

                    {

                        text: ""

                    },

                    {

                        text: [

                            "Semnătura responsabil cu înregistrarea participanților\n\n",
                            "……………………\n\n",

                            "Data:\n\n",

                            "……………………"

                        ],

                        bold: true

                    }

                ]

            },

            {
                text: "\n\n\nSecțiunea C. Statut pe piața muncii la 6 luni de la ieșirea din operațiune\n\n\n",
                decoration: "underline",
                bold: true
            },

            {
                text: [
                    { text: "[ ] ", style: "checkbox" },
                    {
                        text: "Persoană care are un loc de muncă\n"
                    },
                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Persoană care desfășoară o activitate independentă \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoană a cărei situație pe piața forței de muncă s- a îmbunătățit \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Participanți cu vârsta de peste 54 de ani care au un loc de muncă \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Participanți cu vârsta de peste 54 de ani care desfășoară o activitate independentă \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {
                        text: "Persoane dezavantajate care au un loc de muncă \n"
                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Persoane dezavantajate care desfășoară o activitate independentă \n"

                    },

                    { text: "[ ] ", style: "checkbox" },

                    {

                        text:

                            "Altă situație: ...................................................... \n\n\n"

                    }

                ]

            },

            {

                alignment: "justify",

                columns: [

                    {

                        text: ""

                    },

                    {

                        text: [

                            "Semnătura responsabil cu înregistrarea participanților\n\n",
                            "……………………\n\n",

                            "Data:\n\n",

                            "……………………"

                        ],

                        bold: true

                    }

                ]

            }

        ],

        styles: {

            header: {

                fontSize: 8,

                italics: true

            },

            subheader: {

                fontSize: 11,

                italics: true

            },

            checkbox: {

                fontSize: 18

            },

            quote: {

                italics: true

            },

            small: {

                fontSize: 8

            }

        },

        defaultStyle: {

            columnGap: 20

        }
    }

};

export default anexa8;