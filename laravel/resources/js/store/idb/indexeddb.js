const DB_NAME = 'patients';
const DB_VERSION = 1;
let DB;
import moment from 'moment'

export default {

	async getDb() {
		return new Promise((resolve, reject) => {
		
			if(DB) { 
				return resolve(DB); }
			
			let request = window.indexedDB.open(DB_NAME, DB_VERSION);
			
			request.onerror = e => {
				console.log('Error opening db', e);
				reject('Error');
			};
	
			request.onsuccess = e => {
				console.log('success opening db',DB_NAME)
				DB = e.target.result;
				resolve(DB);
			};
			
			request.onupgradeneeded = e => {
				console.log('upgrade needed', DB_NAME);
				let db = e.target.result;
				let objstore = db.createObjectStore("patients", { autoIncrement: true, keyPath:'id' });
				objstore.createIndex('updated','updated_at',{unique:false});
				objstore.createIndex('cnp','cnp',{unique:true});
				
			};
		});
	},
	async getPatient(id){
		let db = await this.getDb();

		return new Promise(resolve => {

			let trans = db.transaction(['patients'],'readonly');
			trans.oncomplete = () => {
				resolve(patient);
			};
			
			let store = trans.objectStore('patients');
			let patient = store.get(id);
		})
	},
	async editPatient(pacient){
		let db = await this.getDb();

		return new Promise(resolve => {

			let trans = db.transaction(['patients'],'readonly');
			trans.oncomplete = () => {
				resolve(patient);
			};
			
			let store = trans.objectStore('patients');
			let patient = store.put(pacient,pacient.id);
		})
	},

	async deletePatient(patient) {

		let db = await this.getDb();

		return new Promise(resolve => {

			let trans = db.transaction(['patients'],'readwrite');
			trans.oncomplete = () => {
				resolve();
			};
			let store = trans.objectStore('patients');
			let patient = store.delete(patient.id);
		});	
	},
	async getPatientsUpdatedSince(date){

		let db = await this.getDb();

		return new Promise(resolve => {

			let trans = db.transaction(['patients'],'readonly');
			trans.oncomplete = () => {
				resolve(patients);
			};

			let store = trans.objectStore('patients');
			let index = store.index('updated');
			let patients = [];

			let formatted_date = moment(date).format("YYYY-MM-DD HH:MM:SS")
			//console.log(formatted_date)

			var range = IDBKeyRange.lowerBound(formatted_date);
			
			index.openCursor(range).onsuccess = e => {
				let cursor = e.target.result;
				if (cursor && patients.length < 10) {
					patients.push(cursor.value)
					cursor.continue();
				}
			};
		})

	},
	async getPatients() {

		let db = await this.getDb();

		return new Promise(resolve => {

			let trans = db.transaction(['patients'],'readonly');
			trans.oncomplete = () => {
				resolve(patients);
			};
			
			let store = trans.objectStore('patients');
			let patients = [];
			
			store.openCursor().onsuccess = e => {
				let cursor = e.target.result;
				if (cursor) {
					patients.push(cursor.value)
					cursor.continue();
				}
			};

		});
	},

	async savePatient(patient) {

		let db = await this.getDb();

		return new Promise(resolve => {

			let trans = db.transaction(['patients'],'readwrite');
			trans.oncomplete = () => {
				resolve();
			};

			let store = trans.objectStore('patients');
			store.put(patient);

		});
	
	},
	
	async saveDatabase(patients) {

		let db = await this.getDb();

		return new Promise(resolve => {

			let trans = db.transaction(['patients'],'readwrite');
			trans.oncomplete = () => {
				resolve();
			};

			let store = trans.objectStore('patients');
			store.clear();
			patients.forEach(element => {
				store.put(element);
			});
		

		});
	
	},

}