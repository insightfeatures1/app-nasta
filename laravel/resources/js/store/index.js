import Vue from 'vue'
import Vuex from 'vuex'
import settings from './modules/settings'
import VuexPersist from 'vuex-persist'
import db from './modules/db'
const vuexPersist = new VuexPersist({
    key: 'nasta',
    storage: window.localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        settings,
        db
    },
    plugins: [vuexPersist.plugin]
})