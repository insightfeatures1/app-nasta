// initial state
const state = {
    color: true,
    sidebar: true
}

// getters
const getters = {
    color: state => state.color,
    sidebar: state => state.sidebar
}

// actions
const actions = {}

// mutations
const mutations = {
    setColor(state, payload) {
        state.color = payload
    },

    setSidebar(state, payload) {
        state.sidebar = payload
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}