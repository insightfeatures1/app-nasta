import idb from '../idb/indexeddb'
export default {
    state: {
        patients: []
    },
    mutations: {

    },
    actions: {
        async deletePatient(context, Patient) {
            await idb.deletePatient(Patient);
        },
        async getPatients(context) {
            let Patients = await idb.getPatients();
            Patients.forEach(c => {
                context.state.patients.push(c);
              });
        },
        async savePatient(context, Patient) {
            await idb.savePatient(Patient);
        }
    }
}