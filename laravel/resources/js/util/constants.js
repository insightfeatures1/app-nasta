const roles = [
    {id:1,name:'Admin',slug:'admin'},
    {id:2,name:'Medic',slug:'medic'},
    {id:5,name:'Partener',slug:'partener'},
    {id:8,name:'Echipa implementare',slug:'echipa-implementare'},
    {id:10,name:'Echipa caravana',slug:'echipa-caravana'},
];

export default { roles };