import moment from "moment";

const calculateGender = (num) => {
    const M = 'M';
    const F = 'F';
    const MArray = [1, 5, 7];
    const FArray = [2, 6, 8];
    
    if (MArray.includes(num)) {
        return M;
    }

    if (FArray.includes(num)) {
        return F;
    }

    return false;
};

const validate = (cnp) => {
    // (1|2|5|6|7|8)G AA MM DD JJ RRR C
    const regex = RegExp(/^([125678])(\d{2})(\d{2})(\d{2})(\d{2})(\d{3})\d/);

    if (!regex.test(cnp)) {
        return false;
    }

    const splitArray = cnp.match(regex);

    if (splitArray === null) {
        return false;
    }

    let parsed = {
        input: splitArray.input,
        genderNum: parseInt(splitArray[1]),
        
        month: parseInt(splitArray[3]),
        day: parseInt(splitArray[4]),
        state: parseInt(splitArray[5])
    };
    switch(parsed.genderNum){
        case 1:
        case 2:
            parsed.year = 1900 + parseInt(splitArray[2]);
            break;
        case 3:
        case 4:
            parsed.year = 1800 + parseInt(splitArray[2]);
            break;
        case 5:
        case 6:
            parsed.year = 2000 + parseInt(splitArray[2]);
            break;
        case 7:
        case 8:
            parsed.year = 1900 + parseInt(splitArray[2]);
            break;
    }
    //console.log(parsed);
    parsed.age = Math.abs(moment(`${parsed.year}-${parsed.month}-${parsed.day}`, "YYYY-M-D").diff(Date(), 'years'));
    parsed.gender = calculateGender(parsed.genderNum);

    return parsed;
}

export default validate;