// /src/plugins/axios.js
import idb from '../store/idb/indexeddb'
import axios from 'axios';

axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`
// doing something with the request
axios.interceptors.request.use(
  (request) => {
    // do something with request meta data, configuration, etc
      // dont forget to return request object,
      // otherwise your app will get no answer
      // if(request.url && request.url.includes('pacients')){
      //   console.log('intercepted',request);
      //   //request param force online
      // }
      return request;
  }
);

// doing something with the response
axios.interceptors.response.use(
    (response) => {
      // if(response.request.responseURL && response.request.responseURL.includes('pacients')){
      //   console.log(response.request)
      // }
      // all 2xx/3xx responses will end here
      //console.log('interceped response',response)
      
      return response;
    },
    (error) => {
        //console.log('inter',error)
        return Promise.reject(error);
      // all 4xx/5xx responses will end here
      // window.intercepted.$on('response', data => {
      //     if (data.body.error && data.body.error === 'Unauthorized') {
      //         console.log('Unauthorized');
      //         Vue.auth.logout();
      //         return window.location = `${process.env.MIX_APP_URL}/login`;
      //         //return Vue.auth.logout();
      //         //return Vue.router.replace({ name: 'login' });
      //     }
      //     return data;
      // })
    }
);

export default axios;


// window.intercepted.$on('response', data => {
//     if (data.body.error && data.body.error === 'Unauthorized') {
//         console.log('Unauthorized');
//         Vue.auth.logout();
//         return window.location = `${process.env.MIX_APP_URL}/login`;
//         //return Vue.auth.logout();
//         //return Vue.router.replace({ name: 'login' });
//     }

//     return data;
// });
//  Vue.axios.interceptors.response.use(
//   response => function(response){
//   },
//   error => function(error) {
//       Vue.auth.logout({
//                 redirect: {name: 'login'}
//         });
//   }
// );
//  Vue.axios.interceptors.push(function(request, next) {
//     next(function (res) {
//         // Unauthorized Access
//         if (
//             res.status === 401 &&
//             ['UnauthorizedAccess', 'InvalidToken'].indexOf(res.data.code) > -1
//         ) {
//             Vue.auth.logout({
//                 redirect: {name: 'auth-login'}
//             });
//         }
//         // System Error
//         else if (res.status === 500) {
//             Vue.router.push({name: 'error-500'});
//         }
//     });
// });
