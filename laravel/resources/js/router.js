import VueRouter from 'vue-router'

// Pages
import Register from './pages/Register'
import Login from './pages/Login'
import Dashboard from './pages/user/Dashboard'

import AdaugaPacient from './pages/user/AdaugaPacient'
import FisaPacient from './pages/user/FisaPacient'
import ListaPacienti from './pages/user/ListaPacienti'
import ListaExpertLocal from './pages/user/ListaExpertLocal'
import ListaEvenimente from './pages/user/ListaEvenimente'
import Calendar from './pages/user/Calendar'
import CalendarAdd from './pages/user/CalendarAdd'
import Eveniment from './pages/user/Eveniment'

import AfisareRadiografie from './pages/user/AfisareRadiografie'

import MobilizareComunitati from './pages/user/MobilizareComunitati'
import MobilizareComunitatiAdd from './pages/user/MobilizareComunitatiAdd'

import AdminStocuriPVAdd from "./pages/admin/StocuriPVAdd"
import AdminStocuriPVView from "./pages/admin/StocuriPVView"

import AdminParcAuto from "./pages/admin/ParcAuto";

import AdminInfoCaravane from './pages/admin/InfoCaravane'
import AdminViewCaravana from './pages/admin/ViewCaravana'
import AdminViewCaravanaDetails from './components/Caravana/Detalii'
import AdminViewCaravanaStocuri from './components/Caravana/Stocuri'

import AdminAddCaravana from './pages/admin/AddCaravana'
import AdminStocuri from './pages/admin/Stocuri'
import AdminPersonal from './pages/admin/Personal'
import AdminUtilizatori from './pages/admin/Utilizatori'

import RaportStocuri from './pages/rapoarte/RaportStocuri'
import RaportPacienti from './pages/rapoarte/RaportPacienti'

import DateContact from './components/DateContact'
import PacientChestionar from './components/Screening/Chestionar'
import PacientRadiologie from './components/Screening/Radiologie/index'
/* import PacientRadiologieList from './components/Screening/Radiologie/List'
import PacientRadiologieView from './components/Screening/Radiologie/View'
import PacientRadiologieAdd from './components/Screening/Radiologie/Add' */

import PacientFormulareGT from './components/PacientFormulareGT'
import PacientAnalize from './components/Screening/Analize'
import PacientInterventii from './components/Interventii'
import PacientTratament from './components/Tratament'

import RegistruDozaRadiatie from './pages/user/RegistruDozaRadiatie'
import RegistruDozaRadiatieAdd from './pages/user/RegistruDozaRadiatieAdd'
import ListaSpute from './pages/user/ListaSpute'


import RouteNotFound from './components/RouteNotFound'
import AccessDenied from './components/AccessDenied'

import BuletinInterpretare from "./components/PDF/BuletinInterpretare";
import BiletTrimitere from "./components/PDF/BiletTrimitere";
import OpisDocumente from "./components/PDF/OpisDocumente";
import Formular8 from "./components/PDF/Formular8";
import Formular4 from "./components/PDF/Formular4";
import Formular5 from "./components/PDF/Formular5";
import Formular7 from "./components/PDF/Formular7";
import Formular7a from "./components/PDF/Formular7a";
import Formular7b from "./components/PDF/Formular7b";
import Formular3 from "./components/PDF/Formular3";
import BuletinSpute from './components/PDF/BuletinSpute';
import PVInchidereTura from './components/PDF/PVInchidereTura';

// Routes
const routes = [
    {
        path: '/pdf/buletin-interpretare',
        name: 'buletin-interpretare',
        component: BuletinInterpretare,
        meta: {
            auth: { roles: ['medic'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Buletin Interpretare',
            crumbs: [
                {
                    link: false,
                    name: 'Buletin Interpretare'
                }
            ]
        }
    },
    {
        path: '/pdf/bilet-trimitere',
        name: 'bilet-trimitere',
        component: BiletTrimitere,
        meta: {
            auth: { roles: ['medic'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Bilet Trimitere',
            crumbs: [
                {
                    link: false,
                    name: 'Bilet Trimitere'
                }
            ]
        }
    },
    {
        path: '/pdf/pv-inchidere-tura',
        name: 'pv-inchidere-tura',
        component: PVInchidereTura,
        meta: {
            auth: { roles: ['echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Proces Verbal inchidere tura',
            crumbs: [
                {
                    link: false,
                    name: 'Proces verbal inchidere tura'
                }
            ]
        }
    },
    {
        path: '/pdf/opis-documente',
        name: 'opis-documente',
        component: OpisDocumente,
        meta: {
            //auth: { roles: ['medic'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            auth: true,
            title: 'Opis Documente',
            crumbs: [
                {
                    link: false,
                    name: 'Opis Documente'
                }
            ]
        }
    },
    {
        path: '/pdf/chestionar-colectare-date-medicale',
        name: 'formular-8',
        component: Formular8,
        meta: {
            //auth: { roles: ['medic'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            auth: true,
            title: 'Chestionar colectare date medicale',
            crumbs: [
                {
                    link: false,
                    name: 'Chestionar colectare date medicale'
                }
            ]
        }
    },
    {
        path: '/pdf/formular-inregistrare',
        name: 'formular-3',
        component: Formular3,
        meta: {
            //auth: { roles: ['medic'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            auth: true,
            title: 'Formularul de înregistrare individuală a participanților',
            crumbs: [
                {
                    link: false,
                    name: 'Formularul de înregistrare individuală a participanților'
                }
            ]
        }
    },
    {
        path: '/pdf/declaratie-evitare',
        name: 'formular-4',
        component: Formular4,
        meta: {
            //auth: { roles: ['medic'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            auth: true,
            title: 'Declaratie evitare a dublei finantari',
            crumbs: [
                {
                    link: false,
                    name: 'Declaratie evitare a dublei finantari'
                }
            ]
        }
    },
    {
        path: '/pdf/angajament-disponibilitate',
        name: 'formular-5',
        component: Formular5,
        meta: {
            //auth: { roles: ['medic'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            auth: true,
            title: 'ANGAJAMENT de disponibilitate față de activitățile proiectului',
            crumbs: [
                {
                    link: false,
                    name: 'ANGAJAMENT de disponibilitate față de activitățile proiectului'
                }
            ]
        }
    },
    {
        path: '/pdf/consimtamant-acord',
        name: 'formular-7',
        component: Formular7,
        meta: {
            //auth: { roles: ['medic'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            auth: true,
            title: 'CONSIMȚĂMÂNT PRIVIND ACORDUL DE PRELUCRARE A DATELOR CU CARACTER PERSONAL',
            crumbs: [
                {
                    link: false,
                    name: 'CONSIMȚĂMÂNT PRIVIND ACORDUL DE PRELUCRARE A DATELOR CU CARACTER PERSONAL'
                }
            ]
        }
    },
    // {
    //     path: '/pdf/anexa-acord',
    //     name: 'formular-7a',
    //     component: Formular7a,
    //     meta: {
    //         auth: true,
    //         title: 'CONSIMȚĂMÂNT PRIVIND ACORDUL DE PRELUCRARE A DATELOR CU CARACTER PERSONAL',
    //         crumbs: [
    //             {
    //                 link: false,
    //                 name: 'CONSIMȚĂMÂNT PRIVIND ACORDUL DE PRELUCRARE A DATELOR CU CARACTER PERSONAL'
    //             }
    //         ]
    //     }
    // },
    // {
    //     path: '/pdf/anexa-acord-2',
    //     name: 'formular-7b',
    //     component: Formular7b,
    //     meta: {
    //         auth: true,
    //         title: 'CONSIMȚĂMÂNT PRIVIND ACORDUL DE PRELUCRARE A DATELOR CU CARACTER PERSONAL',
    //         crumbs: [
    //             {
    //                 link: false,
    //                 name: 'CONSIMȚĂMÂNT PRIVIND ACORDUL DE PRELUCRARE A DATELOR CU CARACTER PERSONAL'
    //             }
    //         ]
    //     }
    // },
    // USER ROUTES
    {
        path: '/',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            auth: true,
            title: 'Dashboard',
            crumbs: [
                {
                    link: false,
                    name: 'Dashboard'
                }
            ]
        }
    },
    // USER ROUTES
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            auth: true,
            title: 'Dashboard',
            crumbs: [
                {
                    link: false,
                    name: 'Dashboard'
                }
            ]
        }
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    },
    {
        path: '/lista-pacienti',
        name: 'lista-pacienti',
        component: ListaPacienti,
        meta: {
            auth: true,
            title: 'Lista pacienti',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Lista pacienti'
                }
            ]
        }
    },
    {
        path: '/mobilizare-comunitati',
        name: 'mobilizare-comunitati',
        component: MobilizareComunitati,
        meta: {
            auth: { roles: ['partener'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Mobilizare comunitati',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Mobilizare comunitati'
                }
            ]
        }
    },
    {
        path: '/mobilizare-comunitati-add',
        name: 'mobilizare-comunitati-add',
        component: MobilizareComunitatiAdd,
        meta: {
            auth: { roles: ['partener'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Mobilizare comunitati',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Mobilizare comunitati'
                }
            ]
        }
    },
    {
        path: '/lista-evenimente',
        name: 'lista-evenimente',
        component: ListaEvenimente,
        meta: {
            auth: { roles: ['partener','echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Coordonare regiune',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Coordonare regiune'
                }
            ]
        }
    },
    {
        path: '/lista-expert-local',
        name: 'lista-expert-local',
        component: ListaExpertLocal,
        meta: {
            auth: { roles: ['medic'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Lista pacienti',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Lista pacienti'
                }
            ]
        }
    },
    {
        path: '/afisare-radiografie/:imageUrl',
        name: 'afisare-radiografie',
        component: AfisareRadiografie,
        meta: {
            auth: true,
            title: 'Radiografie',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Radiografie'
                }
            ]
        }
    },
    {
        path: '/adauga-pacient',
        name: 'adauga-pacient',
        component: AdaugaPacient,
        meta: {
            auth: { roles: ['echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Adauga pacient',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Adauga pacient'
                }
            ]
        }
    },
    {
        path: '/pacient/:id',
        name: 'pacient',
        component: FisaPacient,
        meta: {
            auth: true,
            title: 'Fisa pacient',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: 'lista-pacienti',
                    name: 'Lista pacienti'
                },
                {
                    link: false,
                    name: 'Pacient'
                }
            ]
        },
        children: [
            {
                path: '',
                name: 'pacient-date-contact',
                component: DateContact,
                meta: {
                    auth:  true,
                    title: 'Fisa pacient - date contact',
                    crumbs: [
                        {
                            link: 'dashboard',
                            name: 'Dashboard'
                        },
                        {
                            link: 'lista-pacienti',
                            name: 'Lista pacienti'
                        },
                        // {
                        //     link: 'pacient-date-contact',
                        //     name: 'Pacient'
                        // },
                        {
                            link: false,
                            name: 'Date de contact'
                        }
                    ]
                },
            },
            {
                path: 'chestionar',
                name: 'pacient-chestionar',
                component: PacientChestionar,
                meta: {
                    auth: { roles: ['medic','echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
                    title: 'Fisa pacient - chestionar',
                    crumbs: [
                        {
                            link: 'dashboard',
                            name: 'Dashboard'
                        },
                        {
                            link: 'lista-pacienti',
                            name: 'Lista pacienti'
                        },
                        {
                            link: 'pacient-date-contact',
                            name: 'Pacient'
                        },
                        {
                            link: false,
                            name: 'Screening'
                        },
                        {
                            link: false,
                            name: 'Chestionar'
                        }
                    ]
                }
            },
            {
                path: 'radiologie',
                name: 'pacient-radiologie',
                component: PacientRadiologie,
                meta: {
                    auth: { roles: ['medic','echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
                    title: 'Fisa pacient - radiologie',
                    crumbs: [
                        {
                            link: 'dashboard',
                            name: 'Dashboard'
                        },
                        {
                            link: 'lista-pacienti',
                            name: 'Lista pacienti'
                        },
                        {
                            link: 'pacient-date-contact',
                            name: 'Pacient'
                        },
                        {
                            link: false,
                            name: 'Screening'
                        },
                        {
                            link: false,
                            name: 'Radiologie'
                        }
                    ]
                },
                /*                 children: [
                                    {
                                        path: '',
                                        name: 'pacient-radiologie',
                                        component: PacientRadiologieList,
                                        meta: {
                                            auth: true,
                                            title: 'Fisa pacient - radiologie',
                                            crumbs: [
                                                {
                                                    link: 'dashboard',
                                                    name: 'Dashboard'
                                                },
                                                {
                                                    link: 'lista-pacienti',
                                                    name: 'Lista pacienti'
                                                },
                                                {
                                                    link: false,
                                                    name: 'Pacient'
                                                },
                                                {
                                                    link: false,
                                                    name: 'Screening'
                                                },
                                                {
                                                    link: false,
                                                    name: 'Radiologie'
                                                }
                                            ]
                                        },
                                    },
                                    {
                                        path: 'add',
                                        name: 'pacient-radiologie-add',
                                        component: PacientRadiologieAdd,
                                        meta: {
                                            auth: true,
                                            title: 'Fisa pacient - Radiografie',
                                            crumbs: [
                                                {
                                                    link: 'dashboard',
                                                    name: 'Dashboard'
                                                },
                                                {
                                                    link: 'lista-pacienti',
                                                    name: 'Lista pacienti'
                                                },
                                                {
                                                    link: false,
                                                    name: 'Pacient'
                                                },
                                                {
                                                    link: false,
                                                    name: 'Screening'
                                                },
                                                {
                                                    link: false,
                                                    name: 'Radiologie'
                                                }
                                            ]
                                        },
                                    },
                                    {
                                        path: ':radiografie_id',
                                        name: 'pacient-radiologie-view',
                                        component: PacientRadiologieView,
                                        meta: {
                                            auth: true,
                                            title: 'Fisa pacient - Radiografie',
                                            crumbs: [
                                                {
                                                    link: 'dashboard',
                                                    name: 'Dashboard'
                                                },
                                                {
                                                    link: 'lista-pacienti',
                                                    name: 'Lista pacienti'
                                                },
                                                {
                                                    link: false,
                                                    name: 'Pacient'
                                                },
                                                {
                                                    link: false,
                                                    name: 'Screening'
                                                },
                                                {
                                                    link: false,
                                                    name: 'Radiologie'
                                                }
                                            ]
                                        },
                                    }] */
            },
            {
                path: 'analize',
                name: 'pacient-analize',
                component: PacientAnalize,
                meta: {
                    auth: { roles: ['medic','echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
                    title: 'Fisa pacient - analize',
                    crumbs: [
                        {
                            link: 'dashboard',
                            name: 'Dashboard'
                        },
                        {
                            link: 'lista-pacienti',
                            name: 'Lista pacienti'
                        },
                        {
                            link: 'pacient-date-contact',
                            name: 'Pacient'
                        },
                        {
                            link: false,
                            name: 'Analize'
                        }
                    ]
                }
            },
            {
                path: 'formulare-gt',
                name: 'pacient-formulare-gt',
                component: PacientFormulareGT,
                meta: {
                    auth: { roles: ['medic','echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
                    title: 'Fisa pacient - Formulare GT',
                    crumbs: [
                        {
                            link: 'dashboard',
                            name: 'Dashboard'
                        },
                        {
                            link: 'lista-pacienti',
                            name: 'Lista pacienti'
                        },
                        {
                            link: 'pacient-date-contact',
                            name: 'Pacient'
                        },
                        {
                            link: false,
                            name: 'Formulare GT'
                        }
                    ]
                }
            },
            {
                path: 'interventii',
                name: 'pacient-interventii',
                component: PacientInterventii,
                meta: {
                    auth: { roles: ['medic','partener'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
                    title: 'Fisa pacient - interventii',
                    crumbs: [
                        {
                            link: 'dashboard',
                            name: 'Dashboard'
                        },
                        {
                            link: 'lista-pacienti',
                            name: 'Lista pacienti'
                        },
                        {
                            link: 'pacient-date-contact',
                            name: 'Pacient'
                        },
                        {
                            link: false,
                            name: 'Interventii'
                        }
                    ]
                }
            },
            {
                path: 'tratament',
                name: 'pacient-tratament',
                component: PacientTratament,
                meta: {
                    auth: { roles: ['medic','partener'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
                    title: 'Fisa pacient - tratament',
                    crumbs: [
                        {
                            link: 'dashboard',
                            name: 'Dashboard'
                        },
                        {
                            link: 'lista-pacienti',
                            name: 'Lista pacienti'
                        },
                        {
                            link: 'pacient-date-contact',
                            name: 'Pacient'
                        },
                        {
                            link: false,
                            name: 'Tratament'
                        }
                    ]
                }
            }
        ]
    },
    {
        path: '/lista-spute',
        name: 'lista-spute',
        component: ListaSpute,
        meta: {
            auth: { roles: ['echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Lista Spute',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Lista spute'
                }
            ]
        }
    },
    {
        path: '/raport-stocuri',
        name: 'raport-stocuri',
        component: RaportStocuri,
        meta: {
            auth: { roles: ['echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Raport stocuri',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Raport stocuri'
                }
            ]
        }
    },
    {
        path: '/raport-pacienti',
        name: 'raport-pacienti',
        component: RaportPacienti,
        meta: {
            auth: true,
            title: 'Raport pacienti',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Raport pacienti'
                }
            ]
        }
    },
    {
        path: '/buletin-spute',
        name: 'buletin-spute',
        component: BuletinSpute,
        meta: {
            auth: true,
            title: 'Buletin Spute',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Buletin spute'
                }
            ]
        }
    },
    {
        path: '/registru-doza-radiatie',
        name: 'registru-doza-radiatie',
        component: RegistruDozaRadiatie,
        meta: {
            auth: { roles: ['echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Registru doza radiatie',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Registru doza radiatie'
                }
            ]
        }
    },
    {
        path: '/registru-doza-radiatie-add',
        name: 'registru-doza-radiatie-add',
        component: RegistruDozaRadiatieAdd,
        meta: {
            auth: { roles: ['echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Registru doza radiatie - Adauga',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: 'registru-doza-radiatie',
                    name: 'Registru doza radiatie'
                },
                {
                    link: false,
                    name: 'Adauga'
                }
            ]
        }
    },
    {
        path: '/calendar',
        name: 'calendar',
        component: Calendar,
        meta: {
            auth: { roles: ['echipa-caravana','partener'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Calendar',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Calendar'
                }
            ]
        }
    },
    {
        path: '/calendar/eveniment/:evenimentId',
        name: 'eveniment',
        component: Eveniment,
        meta: {
            auth: { roles: ['echipa-caravana','partener'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Eveniment',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: 'calendar',
                    name: 'Calendar'
                },
                {
                    link: false,
                    name: 'Eveniment'
                }
            ]
        }
    },
    {
        path: '/calendar/add',
        name: 'calendar-add',
        component: CalendarAdd,
        meta: {
            auth: { roles: ['echipa-caravana','partener'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            title: 'Adaugă eveniment',
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: 'calendar',
                    name: 'Calendar'
                },
                {
                    link: false,
                    name: 'Adauga Eveniment'
                }
            ]
        }
    },
    // ADMIN ROUTES
    {
        path: '/admin/parc-auto',
        name: 'admin.parc-auto',
        component: AdminParcAuto,
        meta: {
            title: "Parc auto",
            auth: { roles: ['admin','echipa-implementare'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Parc auto'
                }
            ]
        }
    },
    {
        path: '/admin/auto-add',
        name: 'admin.auto-add',
        component: AdminAddCaravana,
        meta: {
            title: "Adauga Caravane",
            auth: { roles: ['admin','echipa-implementare'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: 'info-caravana',
                    name: 'Info caravane'
                },
                {
                    link: false,
                    name: 'Adauga caravana'
                }
            ]
        }
    },
    {
        path: '/admin/add-caravana',
        name: 'admin.add-caravana',
        component: AdminAddCaravana,
        meta: {
            title: "Adauga Caravane",
            auth: { roles: ['admin','echipa-implementare'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: 'info-caravana',
                    name: 'Info caravane'
                },
                {
                    link: false,
                    name: 'Adauga caravana'
                }
            ]
        }
    },
    {
        path: '/admin/info-caravana',
        name: 'admin.info-caravana',
        component: AdminInfoCaravane,
        meta: {
            title: "Info Caravane",
            auth: { roles: ['admin','echipa-implementare'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Info Caravane'
                }
            ]
        }
    },
    {
        path: '/admin/caravana/:caravanaId',
        name: 'admin.caravana',
        component: AdminViewCaravana,
        meta: {
            auth: { roles: ['admin','echipa-implementare'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
        },
        children: [
            {
                path: '',
                name: 'admin.caravana-details',
                component: AdminViewCaravanaDetails,
                meta: {
                    title: "Detalii Caravana",
                    auth: { roles: "admin", redirect: { name: 'login' }, forbiddenRedirect: '/403' },
                    crumbs: [
                        {
                            link: 'dashboard',
                            name: 'Dashboard'
                        },
                        {
                            link: false,
                            name: 'Info Caravane'
                        },
                        {
                            link: false,
                            name: 'Detalii caravana'
                        }
                    ]
                }
            },
            {
                path: 'stocuri',
                name: 'admin.caravana-stocuri',
                component: AdminViewCaravanaStocuri,
                meta: {
                    title: "Stocuri Caravana",
                    auth: { roles: "admin", redirect: { name: 'login' }, forbiddenRedirect: '/403' },
                    crumbs: [
                        {
                            link: 'dashboard',
                            name: 'Dashboard'
                        },
                        {
                            link: false,
                            name: 'Info Caravane'
                        },
                        {
                            link: false,
                            name: 'Detalii caravana - Stocuri'
                        }
                    ]
                }
            },
        ]
    },
    {
        path: '/admin/personal',
        name: 'admin.personal',
        component: AdminPersonal,
        meta: {
            title: "Administrare Personal",
            auth: { roles: ['admin','echipa-implementare'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Administrare personal'
                }
            ]
        }
    },
    {
        path: '/admin/utilizatori',
        name: 'admin.utilizatori',
        component: AdminUtilizatori,
        meta: {
            title: "Administrare Utilizatori",
            auth: { roles: ['admin','echipa-implementare'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Administrare utilizatori'
                }
            ]
        }
    },
    {
        path: '/admin/stocuri',
        name: 'admin.stocuri',
        component: AdminStocuri,
        meta: {
            title: "Stocuri",
            auth: { roles: ['admin','echipa-implementare'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Stocuri'
                }
            ]
        }
    },
    {
        path: '/admin/stocuri-pv-add',
        name: 'admin.stocuri-pv-add',
        component: AdminStocuriPVAdd,
        meta: {
            title: "Proces Verbal Caravana",
            auth: { roles: ['echipa-caravana'], redirect: { name: 'login' }, forbiddenRedirect: '/403' },
            crumbs: [
                {
                    link: 'dashboard',
                    name: 'Dashboard'
                },
                {
                    link: false,
                    name: 'Stocuri'
                },
                {
                    link: false,
                    name: 'PV'
                }
            ]
        }
    },
    {
        // will match everything
        path: '/403',
        name: '403',
        component: AccessDenied
    },
    {
        // will match everything
        path: '*',
        name: '404',
        component: RouteNotFound
    }
]

const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,

})

export default router
