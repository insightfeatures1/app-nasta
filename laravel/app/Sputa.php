<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Sputa extends Model
{
    use LogsActivity;

    protected $fillable = [
        'pacient_id',
        'rezultat',
        'genexpert',
        'buletin',
        'prelevata_at',
        'created_by',
        'updated_by'
    ];

    protected static $logAttributes = ["*"];

    function pacient()
    {
        return $this->belongsTo('\App\Pacient', 'pacient_id', 'id');
    }
}
