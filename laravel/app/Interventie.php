<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interventie extends Model
{
    protected $fillable = [
        'pacient_id',
        'data_interventie',
        'durata_apel',
        'doze_omise',
        'reactii_adverse',
        'simptome_depresive',
        'simptome_anxioase',
        'consum_alcool',
        'consum_tutun',
        'alt_comportament_adictiv',
        'boli_existente',
        'referinte_emd',
        'probleme_sociale',
        'created_by',
        'observatii'
    ];

    protected $dates = [
        'data_interventie'
    ];

    public $appends = ['data', 'ora'];

    public function getDataAttribute()
    {
        if ($this->data_interventie !== null) {
            return $this->data_interventie->format('d.m.Y');
        }
    }

    public function getOraAttribute()
    {
        if ($this->data_interventie !== null) {
            return $this->data_interventie->format('h:i');
        }
    }
}
