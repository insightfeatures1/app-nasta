<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Radiologie extends Model
{
    use LogsActivity;

    protected $fillable = [
        'pacient_id',
        'image_url',
        'thumbnail_url',
        'description',
        'created_by'
    ];

    protected static $logAttributes = ["*"];
}
