<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MobilizareComunitati extends Model
{
    use LogsActivity;

    protected $fillable = [
        'judet',
        'localitati',
        'file_url',
        'perioada',
        'created_by'
    ];

    protected $with = ['owner', 'judet'];

    public function owner() {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function judet() {
        return $this->hasOne('App\County', 'id', 'judet');
    }
}
