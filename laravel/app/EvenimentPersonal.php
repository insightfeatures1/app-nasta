<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class EvenimentPersonal extends Pivot
{
    protected $fillable = ['personal_id', 'eveniment_id'];
}
