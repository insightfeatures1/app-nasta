<?php

namespace App\Imports;

use App\Pacient;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToArray;

class PacientsImport implements ToArray, WithProgressBar, WithBatchInserts, WithChunkReading, WithHeadingRow
{
    use Importable;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function array(array $rows)
    {

        foreach ($rows as $row) {
            if ($row['cnp'] !== null && strlen($row['cnp']) == 13) {
                $row['nume'] = $row['nume'];
                $row['prenume'] = $row['prenume'];
                $row['cnp'] = $row['cnp'];
                $row['email'] = $row['e_mail'];
                $row['telefon'] = $row['telefon'];
                $row['gen'] = $row['gen'];
                $row['varsta'] = $row['varstaani'];
                $row['adresa_domiciliu'] = $row['adresa_domiciliu'];
                $row['adresa_resedinta'] = $row['adresa_resedinta'];
                $row['judet_domiciliu'] = $row['judet_domiciliu'];
                $row['judet_resedinta'] = $row['judet_resedinta'];
                $row['zona_domiciliu'] = $row['zona_de_domiciliu'];
                $row['zona_resedinta'] = $row['zona_de_resedinta'];

                $haystack = [
                    "Participanți care trăiesc în gospodării fără persoane ocupate",
                    "Participanți care trăiesc în gospodării fără persoane ocupate cu copii aflați în întreținere",
                    "Participanți care trăiesc în gospodării alcătuite dintr-un părinte unic cu copii aflați în întreținere",
                    "Migranți",
                    "Participanti de origine straina",
                    "Minoritati de etnie Roma",
                    "Alte minoritati decat cele de etnie Roma",
                    "Persoane cu dizabilitati",
                    "Alte categorii de persoane dezavantajate",
                    "Persoane fără adăpost sau care sunt afectate de excluziunea locativă",
                    "Aparțin la comunități marginalizate"
                ];

                $studii = [
                    "ISCED 0" => "Studii-Educatie-timpurie-(ISCED-0)",
                    "ISCED 4" => "Studii-postliceale-(ISCED-4)",
                    "ISCED 7" => "Studii-superioare-(ISCED-7)",
                    "ISCED 1" => "Studii-primare-(ISCED-1)",
                    "ISCED 5" => "Studii-superioare-(ISCED-5)",
                    "ISCED 8" => "Studii-superioare-(ISCED-8)",
                    "ISCED 2" => "Studii-gimnaziale-(ISCED-2)",
                    "ISCED 6" => "Studii-superioare-(ISCED-6)",
                    "Fara" => "fara-ISCED",
                    "ISCED 3" => "Studii-liceale-(ISCED-3)"
                ];

                foreach ($haystack as $h) {
                    if ($row[str_slug($h, '_')] == 'Da') {
                        $row['p_d_extra'] = str_slug($h);
                    }
                }

                $row['nivelul_studiilor'] = $studii[$row['nivelul_studiilor']];

                $row['informatii_generale'] = json_encode($row);


                $pacient = Pacient::create($row);

                return $pacient;
            }
        }
        /* var_dump($row);

        return null;

        if ($row['cnp'] !== null) {
          
            return new Pacient([
                'nume' => $row['nume'],
                'prenume' => $row['prenume'],
                'cnp' => $row['cnp'],
                'email' => $row['e_mail'],
                'telefon' => $row['telefon'],
                'gen' => $row['gen'],
                'varsta' => $row['varstaani'],
                'adresa_domiciliu' => $row['adresa_domiciliu'],
                'adresa_resedinta' => $row['adresa_resedinta'],
                'judet_domiciliu' => $row['judet_domiciliu'],
                'judet_resedinta' => $row['judet_resedinta'],
                'informatii_generale' => json_encode($row),
            ]);
        } else {
            return null;
        } */
    }

    public function headingRow(): int
    {
        return 6;
    }

    public function batchSize(): int
    {
        return 10;
    }

    public function chunkSize(): int
    {
        return 1;
    }
}
