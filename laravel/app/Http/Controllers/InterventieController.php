<?php

namespace App\Http\Controllers;

use App\Interventie;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InterventieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($pacient_id)
    {
        $interventii = Interventie::where('pacient_id', $pacient_id)->orderBy('id', 'DESC')->get();

        return response()->json(['interventii' => $interventii]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'data' => 'required',
            'durata_apel' => 'required',
            'doze_omise' => 'required',
            'reactii_adverse' => 'required',
            'simptome_depresive' => 'required',
            'simptome_anxioase' => 'required',
            'consum_alcool' => 'required',
            'consum_tutun' => 'required',
            'alt_comportament_adictiv' => 'required',
            'boli_existente' => 'required',
            'referinte_emd' => 'required',
            'probleme_sociale' => 'required'
        ]);

        $dc = explode("T", $request->data);

        $all = $request->all();
        $all['data_interventie'] = Carbon::parse($dc[0] . " " . $request->ora['HH'].":".$request->ora['mm']);
        $all['data'] = Carbon::parse($dc[0] . " " . $request->ora['HH'] . ":" . $request->ora['mm']);
        $all['created_by'] = auth()->user()->id;
        $interventie = Interventie::create($all);

        return response()->json(['interventie' => $interventie]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Interventie  $interventie
     * @return \Illuminate\Http\Response
     */
    public function show(Interventie $interventie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Interventie  $interventie
     * @return \Illuminate\Http\Response
     */
    public function edit(Interventie $interventie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Interventie  $interventie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Interventie $interventie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Interventie  $interventie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Interventie $interventie)
    {
        //
    }
}
