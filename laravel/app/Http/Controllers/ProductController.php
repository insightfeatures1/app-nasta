<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();

        return response()->json(
            [
                'status' => 'success',
                'products' => $products->toArray()
            ],
            200
        );
    }

    public function show(Request $request, $id)
    {
        $product = Product::find($id);

        return response()->json(
            [
                'status' => 'success',
                'product' => $product->toArray()
            ],
            200
        );
    }
    public function add(Request $request)
    {
        $this->validate($request, [
            '*' => 'required'
        ]);

        $product = Product::where('denumire', $request->denumire)->where('categorie', $request->categorie)->first();

        if ($product !== null) {
            $product->stoc = $product->stoc + $request->stoc;
            $product->save();
        } else {
            $product = Product::create($request->all());
        }
        return response()->json(
            [
                'status' => 'success',
                'product' => $product->toArray()
            ],
            200
        );
    }

    public function updateStoc(Request $request)
    {
        $product = Product::where('id', $request->product_id)->first();

        if ($product !== null) {
            $product->stoc = $product->stoc + $request->quantity;
            $product->save();
        }

        return response()->json(
            [
                'status' => 'success',
                'product' => $product->toArray()
            ],
            200
        );
    }

    public function update(Request $request, $id)
    {

        $product = Product::findOrFail($id);

        $product->update($request->all());

        return response()->json(
            [
                'status' => 'success',
                'product' => $product->toArray()
            ],
            200
        );
    }

    public function remove($id)
    {

        $product = Product::findOrFail($id);
        $product->delete();

        return response()->json(
            [
                'status' => 'success'
            ],
            200
        );
    }
}
