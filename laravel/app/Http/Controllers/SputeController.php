<?php

namespace App\Http\Controllers;

use App\Sputa;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SputeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spute = Sputa::with('pacient')->orderBy('id', 'DESC')->get();

        return response()->json(['spute' => $spute]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sputa  $sputa
     * @return \Illuminate\Http\Response
     */
    public function show(Sputa $sputa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sputa  $sputa
     * @return \Illuminate\Http\Response
     */
    public function edit(Sputa $sputa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sputa  $sputa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $all = $request->all();
        $all['updated_by'] = auth()->user()->id;

        if ($request->prelevata == true) {
            $all['prelevata_at'] = Carbon::now();
        }

        if ($request->id) {
            $sputa = Sputa::where('id', $request->id)->first();

            if ($sputa == null) {
                $sputa = Sputa::create($all);
            } else {
                $sputa->update($all);
            }
        } else {
            $all['created_by'] = auth()->user()->id;
            $sputa = Sputa::create($all);
        }

        return response()->json(['sputa' => $sputa]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sputa  $sputa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sputa $sputa)
    {
        //
    }
}
