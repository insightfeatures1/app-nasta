<?php

namespace App\Http\Controllers;

use App\DozaRadiatie;
use Illuminate\Http\Request;

class DozeRadiatieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $start = $request->query('start_date');
        $end = $request->query('end_date');
        $query = DozaRadiatie::with('pacient')->orderBy('id', 'DESC');
        if($start && $end) {
            $query->where('created_at', '>=', $start . " 00:00:00");
            $query->where('created_at', '<=', $end . " 23:59:59");
        }
        $doze = $query->get();

        return response()->json(['doze' => $doze]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'doza.pacient_id' => 'required',
            'doza.inaltime' => 'required',
            'doza.greutate' => 'required',
            'doza.kv' => 'required',
            'doza.ma' => 'required',
            'doza.timp_exp' => 'required',
            'doza.mas' => 'required',
            'doza.distanta_focar' => 'required',
            'doza.esak' => 'required',
        ]);

        $all = $request->all();
        $all['doza']['created_by'] = auth()->user()->id;

        $doza = DozaRadiatie::create($all['doza']);

        return response()->json(['doza' => $doza]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DozaRadiatie  $dozaRadiatie
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doza = DozaRadiatie::where('id', $id)->first();

        return response()->json(['doza' => $doza]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DozaRadiatie  $dozaRadiatie
     * @return \Illuminate\Http\Response
     */
    public function edit(DozaRadiatie $dozaRadiatie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DozaRadiatie  $dozaRadiatie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DozaRadiatie $dozaRadiatie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DozaRadiatie  $dozaRadiatie
     * @return \Illuminate\Http\Response
     */
    public function destroy(DozaRadiatie $dozaRadiatie)
    {
        //
    }
}
