<?php

namespace App\Http\Controllers;

use App\Radiologie;
use Illuminate\Http\Request;

class RadiologieController extends Controller
{
    public function index()
    {
        $radiologii = Radiologie::all();

        return response()->json(
            [
                'status' => 'success',
                'radiologii' => $radiologii->toArray()
            ],
            200
        );
    }

    public function view(Request $request, $id)
    {
        $radiografie = Radiologie::where('id', $id)->first();

        return response()->json(
            [
                'status' => 'success',
                'radiografie' => $radiografie
            ],
            200
        );
    }

    public function show(Request $request, $id)
    {
        $radiologii = Radiologie::where('pacient_id', $id)->get();

        return response()->json(
            [
                'status' => 'success',
                'radiologii' => $radiologii->toArray()
            ],
            200
        );
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'file' => 'required',
            'description' => 'required'
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->move(public_path() . "/../../photos/", $file->getClientOriginalName());
            $url = url("/photos/" . $path->getFilename());

            

            $media = Radiologie::create([
                'pacient_id' => $request->pacient_id,
                'image_url' => $url,
                'thumbnail_url' => $url,
                'created_by' => auth()->user()->id,
                'description' => $request->description
            ]);

            return response()->json(['radiologie' => $media]);
        } else {
            return response()->json(['message' => 'File not found in request'], 400);
        }
    }

    public function update(Request $request, $id)
    {

        $product = Product::findOrFail($id);

        $product->update($request->all());

        return response()->json(
            [
                'status' => 'success',
                'product' => $product->toArray()
            ],
            200
        );
    }

    public function remove($id)
    {

        $product = Product::findOrFail($id);
        $product->delete();

        return response()->json(
            [
                'status' => 'success'
            ],
            200
        );
    }
}
