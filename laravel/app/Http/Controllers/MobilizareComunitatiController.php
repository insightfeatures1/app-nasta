<?php

namespace App\Http\Controllers;

use App\MobilizareComunitati;
use Illuminate\Http\Request;

class MobilizareComunitatiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $elemente = MobilizareComunitati::all();

        return response()->json(['elemente' => $elemente]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judet' => 'required',
            'localitati' => 'required',
            'file' => 'required'
        ]);

        $all = $request->all();
        $all['created_by'] = auth()->user()->id;

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->move(public_path() . "/../../photos/", $file->getClientOriginalName());
            $url = url("/photos/" . $path->getFilename());

            $media = MobilizareComunitati::create([
                'file_url' => $url,
                'created_by' => auth()->user()->id,
                'judet' => $request->judet,
                'localitati' => $request->localitati,
                'perioada' => $request->perioada
            ]);

            return response()->json($media);
        } else {
            return response()->json(['message' => 'File not found in request'], 400);
        }

        $element = MobilizareComunitati::create($all);

        return response()->json($element);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MobilizareComunitati  $mobilizareComunitati
     * @return \Illuminate\Http\Response
     */
    public function show(MobilizareComunitati $mobilizareComunitati)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MobilizareComunitati  $mobilizareComunitati
     * @return \Illuminate\Http\Response
     */
    public function edit(MobilizareComunitati $mobilizareComunitati)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MobilizareComunitati  $mobilizareComunitati
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MobilizareComunitati $mobilizareComunitati)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MobilizareComunitati  $mobilizareComunitati
     * @return \Illuminate\Http\Response
     */
    public function destroy(MobilizareComunitati $mobilizareComunitati)
    {
        //
    }
}
