<?php

namespace App\Http\Controllers;

use App\ProdusCaravana;
use App\StocuriPV;
use Illuminate\Http\Request;

class StocuriPVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'produse' => 'required',
            'caravana_id' => 'required'
        ]);

        $productsToAdd = [];

        foreach ($request->produse as $key => $prod) {
            $produs = ProdusCaravana::where('product_id', $prod['product_id'])->first();

            if (isset($prod['quantity'])) {
                $produs->quantity = $produs->quantity - $prod['quantity'];
                $produs->save();
                $productsToAdd[] = $prod;
            }
        }

        $element = StocuriPV::create([
            'created_by' => auth()->user()->id,
            'caravana_id' => $request->caravana_id,
            'produse' => json_encode($productsToAdd)
        ]);

        return response()->json(['element' => $element]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StocuriPV  $stocuriPV
     * @return \Illuminate\Http\Response
     */
    public function show(StocuriPV $stocuriPV)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StocuriPV  $stocuriPV
     * @return \Illuminate\Http\Response
     */
    public function edit(StocuriPV $stocuriPV)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StocuriPV  $stocuriPV
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StocuriPV $stocuriPV)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StocuriPV  $stocuriPV
     * @return \Illuminate\Http\Response
     */
    public function destroy(StocuriPV $stocuriPV)
    {
        //
    }
}
