<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdusCaravanasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('produs_caravanas') ) {
            Schema::create('produs_caravanas', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id');
                $table->integer('caravana_id');
                $table->string('action_type');
                $table->integer('created_by');
                $table->integer('quantity');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produs_caravanas');
    }
}
