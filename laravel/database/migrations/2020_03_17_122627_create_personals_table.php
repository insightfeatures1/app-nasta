<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('personals') ) {
            Schema::create('personals', function (Blueprint $table) {
                $table->increments('id');
                $table->string('echipa', 100)->nullable()->default(NULL);
                $table->string('nume', 100);
                $table->string('functie', 100)->nullable()->default(NULL);
                $table->string('telefon', 100)->nullable()->default(NULL);
                $table->string('fax', 100)->nullable()->default(NULL);
                $table->string('email', 100)->nullable()->default(NULL);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personals');
    }
}
