<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoCaravanasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('info_caravanas') ) {
        Schema::create('info_caravanas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sofer_nume', 100)->nullable();
            $table->string('sofer_prenume', 100)->nullable();
            $table->string('numar_inmatriculare', 40)->nullable();
            $table->timestamp('exp_itp')->nullable();
            $table->timestamp('exp_rca')->nullable();
            $table->string('locatie', 200)->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_caravanas');
    }
}
