<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuletinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('buletins') ) {
        Schema::create('buletins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pacient_id');
            $table->string('type');
            $table->string('rezultat');
            $table->timestamp('data_interpretare');
            $table->text('recomandari');
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buletins');
    }
}
