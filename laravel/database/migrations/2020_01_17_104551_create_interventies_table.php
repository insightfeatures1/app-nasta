<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterventiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('interventies') ) {
        Schema::create('interventies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pacient_id');
            $table->timestamp('data_interventie');
            $table->integer('durata_apel');
            $table->string('doze_omise')->nullable()->default(null);
            $table->string('reactii_adverse')->nullable()->default(null);
            $table->string('simptome_depresive')->nullable()->default(null);
            $table->string('simptome_anxioase')->nullable()->default(null);
            $table->string('consum_alcool')->nullable()->default(null);
            $table->string('consum_tutun')->nullable()->default(null);
            $table->string('alt_comportament_adictiv')->nullable()->default(null);
            $table->string('boli_existente')->nullable()->default(null);
            $table->string('referinte_emd')->nullable()->default(null);
            $table->string('probleme_sociale')->nullable()->default(null);
            $table->string('observatii')->nullable()->default(null);
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interventies');
    }
}
