<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBuletinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buletins', function (Blueprint $table) {
            //
            $table->string('meta', 191)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('buletins', 'meta'))
        {
            Schema::table('buletins', function (Blueprint $table)
            {
               $table->dropColumn('meta');
            });
        }
    }
}
