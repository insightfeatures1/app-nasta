<?php

use Illuminate\Database\Seeder;

class RadiologieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oldUrl="https://app-nasta.dev.itup.ro";
        $newUrl="http://localhost";
        //
        DB::statement(`UPDATE radiologies 
        SET image_url= replace(image_url, '` . $oldUrl . `', '` . $newUrl . `') 
        WHERE image_url  like '%` . $oldUrl . `%'`);
    }
}
